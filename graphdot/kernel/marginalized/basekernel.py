#!/usr/bin/env python
# -*- coding: utf-8 -*-

raise RuntimeError(
    "The basekernel module has been relocated to graphdot.kernel.basekernel.\n"
    "Please update relevant import statements from\n"
    "'from graphdot.kernel.marginalized.basekernel import *'\n"
    "to\n"
    "'from graphdot.kernel.basekernel import *'"
)
