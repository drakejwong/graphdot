#!/usr/bin/env python
# -*- coding: utf-8 -*-
from unittest.mock import MagicMock

init = MagicMock()
managed_empty = MagicMock()
managed_empty_like = MagicMock()
managed_zeros = MagicMock()
mem_attach_flags = MagicMock()
