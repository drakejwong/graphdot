Quick Start Tutorial
====================

Marginalized Graph kernel
-------------------------

This quick-start guide here assumes that the reader is already familiar with the marginalized graph kernel algorithm [Kashima, H., Tsuda, K., & Inokuchi, A. (2003). Marginalized kernels between labeled graphs. *In Proceedings of the 20th international conference on machine learning (ICML-03)* (pp. 321-328).]. Otherwise, please refer to :ref:`user_guide`.
